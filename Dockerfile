FROM ubuntu:bionic

LABEL Maintainer="oldhu <me@oldhu.com>" \
      Description="Discuz! Q container with Nginx & PHP-FPM based on Ubuntu bionic."

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Asia/Shanghai

RUN apt-get update &&  \
    apt-get install -y tzdata apt-utils && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && \
    apt-get install -y --no-install-recommends mysql-server ca-certificates php-fpm nginx cron openssl php-mysql php-gd php-bcmath php-mbstring php-xml php-curl && \
    rm -rf /var/lib/apt/lists/*

COPY root /

RUN chown -R www-data:www-data /var/www/discuz

VOLUME /var/lib/mysql

EXPOSE 80 443

CMD ["/usr/sbin/supervisord", "-c", "/etc/supervisord.conf"]
