#!/bin/sh

DATA_DIR=""
INIT_FILE="--init-file=/etc/init-root-password.sql"

if [ -d "/var/lib/mysqldb" ]; then
    # 没有mysql库，说明这个目录里的数据库是有问题的，可以覆盖
    if [ ! -d "/var/lib/mysqldb/mysql" ]; then 
        cp -r /var/lib/mysql/* /var/lib/mysqldb/
        chown -R mysql:mysql /var/lib/mysqldb
    fi
    DATA_DIR="--datadir=/var/lib/mysqldb"

    if [ ! -f "/var/lib/mysqldb/root.init.lock" ]; then
        touch /var/lib/mysqldb/root.init.lock
    else
        INIT_FILE=""
    fi
fi

if [ ! -L /dev/stdout.err ]; then
    ln -s /dev/stdout /dev/stdout.err
    chmod 666 /dev/stdout.err
fi

[ ! -d /var/run/mysqld ] && mkdir -p /var/run/mysqld
chown mysql:mysql /var/run/mysqld

/usr/sbin/mysqld $DATA_DIR $INIT_FILE --log-error=/dev/stdout